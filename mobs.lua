
local S = minetest.get_translator("icetower");

mobs:register_mob("icetower:guard", {
	type = "monster",
	passive = false,
	damage = 35,
	attack_type = "dogfight",
	group_attack = true,
	shoot_interval = 0.5,
	arrow = "icetower:ice_arrow",
	shoot_offset = 2,
	hp_min = 10,
	hp_max = 25,
	armor = 80,
	collisionbox = {-0.5, -1.5, -0.5, 0.5, 0.5, 0.5},
	visual = "mesh",
	mesh = "zmobs_mese_monster.x",
	textures = {
		{"icetower_guard.png"},
	},
	blood_texture = "icetower_icicle.png",
	makes_footstep_sound = false,
	sounds = {
		random = "mobs_mesemonster",
	},
	view_range = 20,
	walk_velocity = 2.5,
	run_velocity = 10,
	reach = 4,
	stay_near = {"icetower:spawner", 2},
	fly = true,
	fly_in = "air",
	drops = {
		{name = "snow:block", chance = 2, min = 0, max = 2},
	},
	water_damage = 1,
	lava_damage = 1,
	light_damage = 0,
	animation = {
		speed_normal = 15,
		speed_run = 15,
		stand_start = 0,
		stand_end = 14,
		walk_start = 15,
		walk_end = 38,
		run_start = 40,
		run_end = 63,
		punch_start = 40,
		punch_end = 63,
	},
})

mobs:register_arrow("icetower:ice_arrow", {
	visual = "sprite",
	visual_size = {x = 0.5, y = 0.5},
	textures = {"icetower_icicle.png"},
	velocity = 6,

	hit_player = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 2},
		}, nil)
	end,

	hit_mob = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 2},
		}, nil)
	end,

	hit_node = function(self, pos, node)
	end
})

minetest.register_node("icetower:spawner", {
    description = S("Icetower Spawner"),
    drawtype = "airlike",
    paramtype = "light",
    sunlight_propagates = true,
    walkable = false,
    pointable = false,
    diggable = false,
    buildable_to = true,
    drop = "",
})

mobs:spawn({
	name = "icetower:guard",
	nodes = {"icetower:spawner"},
	interval = 5,
	chance = 3,
	active_object_count = 12,
})

