
local schematic_path = minetest.get_modpath("icetower").."/schematics/"

function icetower.add_to_biome(biome, def)

	local icetower_def =  {
		name = biome..":icetower",
		deco_type = "schematic",
		place_on = { "snow:block" },
		place_offset_y = -3,
		sidelen = 64,
		fill_ratio = 0.0002,
		biomes = { biome },
		y_max = 31000,
		y_min = 70,
		schematic = schematic_path.."icetower.mts",
		flags = "place_center_x, place_center_z",
		rotation = "random",
	}

	def = def or {}
	for k, v in pairs(def) do icetower_def[k] = v end

	minetest.register_decoration(icetower_def)
end

